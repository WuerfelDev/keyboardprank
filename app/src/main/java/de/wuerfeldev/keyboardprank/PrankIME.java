package de.wuerfeldev.keyboardprank;

import android.inputmethodservice.InputMethodService;
import android.view.inputmethod.EditorInfo;

public class PrankIME extends InputMethodService {


    @Override
    public void onUpdateExtractingVisibility(EditorInfo ei) {
        super.onUpdateExtractingVisibility(ei);
        setExtractViewShown(false);
    }
}
